package jp.alhinc.hayakawa_mitsuhiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) throws IOException{
		System.out.println("ここにあるファイルを開きます =>" + args[0]);
		BufferedReader br = null;
		BufferedReader pr = null;
		BufferedReader cr = null;
		BufferedWriter bw = null;
		BufferedWriter cbw = null;
		//値を保持するためのmapを宣言:monetMap→支店コードと金額を格納、branchMap→支店コードと支店名を格納
		HashMap<String, Long> moneyMap = new HashMap<String, Long>();
		HashMap<String, String> branchMap = new HashMap<String, String>();
		HashMap<String, String> commodityMap = new HashMap<String, String>();
		HashMap<String, Long> commodityMoneyMap = new HashMap<String, Long>();

		try{
			//branch.lstを読み込む
			String[] branch = null;
			File branchFile = new File(args[0],"branch.lst");
			//branchファイルの存在確認
			if(branchFile.exists()){
				//branchファイルが存在した場合ファイルを読み込む
				FileReader brfr = new FileReader(branchFile);
				br = new BufferedReader(brfr);
				//ファイル内のデータを格納する変数brLineを宣言
				String brLine = null;
				//ファイル内の値がなくなるまでデータをbrlineに代入
				while((brLine = br.readLine()) != null){
					//ファイル内のフォーマットが3桁の数字,～支店かどうかをチェック
					if(brLine.matches("\\d{3},.+支店")){
						//ファイル内のフォーマットが3桁の数字,～支店だった場合以下を処理
						//ファイル内の値をカンマ区切りで分割
						branch = brLine.split(",");
						//分割したデータ(支店コードと支店名)をmapへ格納
						branchMap.put(branch[0], branch[1]);
						//支店コードと金額をmapへ格納
						moneyMap.put(branch[0],(long) 0);
					} else {
						//ファイル内のフォーマットが3桁の数字,～支店でなかった場合エラーメッセージを表示
						System.out.println("支店定義ファイルのフォーマットが不正です");
						//処理を終了
						System.exit(0);
					}
				}
			} else {
				//ファイルが存在しなかったらエラー文を表示
				System.out.println("支店定義ファイルが存在しません");
				System.exit(0);
			}
		} catch(IOException e){
			System.out.println("closeできませんでした。");
		} finally {
			br.close();
		}
		//entrysetでmapの中身を確認
		System.out.println(branchMap.entrySet());
		try{
			//comodity.lstを読み込む
			String[] commodity = null;
			File commodityFile = new File(args[0],"commodity.lst");
			//branchファイルの存在確認
			if(commodityFile.exists()){
				//commodityファイルが存在した場合ファイルを読み込む
				FileReader cmfr = new FileReader(commodityFile);
				cr = new BufferedReader(cmfr);
				//ファイル内のデータを格納する変数crLineを宣言
				String crLine = null;
				//ファイル内の値がなくなるまでデータをcrlineに代入
				while((crLine = cr.readLine()) != null){
					//ファイル内のフォーマットをチェック
					if(crLine.matches("[a-zA-Z]{3}[0-9]{5},.+")){
						//ファイル内のフォーマットが正しい場合以下を処理
						//ファイル内の値をカンマ区切りで分割
						commodity = crLine.split(",");
						//分割したデータ(支店コードと支店名)をmapへ格納
						commodityMap.put(commodity[0], commodity[1]);
						//支店コードと金額をmapへ格納
						commodityMoneyMap.put(commodity[0],(long) 0);
					} else {
						//ファイル内のフォーマットが3桁の数字,～支店でなかった場合エラーメッセージを表示
						System.out.println("商品定義ファイルのフォーマットが不正です");
						//処理を終了
						System.exit(0);
					}
				}
			} else {
				//ファイルが存在しなかったらエラー文を表示
				System.out.println("商品定義ファイルが存在しません");
				System.exit(0);
			}
		} catch(IOException e){
			System.out.println("closeできませんでした。");
		} finally {
			br.close();
		}
		System.out.println(commodityMap.entrySet());
		System.out.println(commodityMoneyMap.entrySet());
		//売上ファイルの検索と集計処理
		try{
			ArrayList<Integer> fileNameList = new ArrayList<Integer>();
			File profitFile = new File(args[0]);
			//ファイルの一覧を取得
			File[] profitList = profitFile.listFiles();
			//ファイルの数だけ処理をまわす
			for(int i = 0; i < profitList.length; i++){
				//ファイル内のデータを格納するlistを宣言
				ArrayList<String> profitArray = new ArrayList<String>();
				//ファイル名を取得
				String fileName = profitList[i].getName();
				//ファイル名が8桁のrcdファイルかどうかを判定
				if(fileName.matches("\\d{8}.rcd$")){
					//ファイル名から.がある位置を取得
					int point = fileName.lastIndexOf(".");
					//ファイル名から拡張子を除いた名前を取得
					String newFileName = fileName.substring(0, point);
					fileNameList.add(Integer.parseInt(newFileName));
					for (int j = 0; j < fileNameList.size() -1 ; j++) {
						int n = fileNameList.get(j);
						int o = fileNameList.get(j + 1);
						// 連続したアドレスの中身を減算した場合に1じゃなかった場合
						if (o - n != 1) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
					}
					//ファイル名が8桁の数字.rcdだった場合ファイル内のデータを読み込む
					FileReader prfr = new FileReader(profitList[i]);
					pr = new BufferedReader(prfr);
					//ファイル内のデータを入れるための変数prLineを宣言
					String prLine = null;
					//ファイル内の行数を格納する変数を宣言
					int fileLine = 0;
					//ファイル内の値がなくなるまでデータをprLineに代入
					while((prLine = pr.readLine()) != null){
						//ファイル内のデータを1行読む毎に行数をインクリメント
						fileLine++;
						//ファイル内のデータが4行になったらエラー文を表示
						if(fileLine == 4){
							System.out.println(fileName + "のフォーマットが不正です");
							System.exit(0);
						} else {
						//代入されたデータをlistへ格納
						profitArray.add(prLine);
						}
					}
					if(branchMap.containsKey(profitArray.get(0))){
						//もしmoneyMapとprofitArrayに同じキーがあればmoneyMapの金額とprofitArrayの金額を足す
						Long result =  moneyMap.get(profitArray.get(0)) + Long.parseLong(profitArray.get(2));
						//足した金額が10桁を超えたらエラー文を表示
						if(String.valueOf(result).matches("[0-9]{10,}")){
							System.out.println("合計金額が10桁を超えました");
							System.exit(0);
						} else {
						//足した値をMapへ格納
						moneyMap.put(profitArray.get(0), result);
						}
					} else {
						System.out.println(fileName + "の支店コードが不正です");
						System.exit(0);
					}
					if(commodityMap.containsKey(profitArray.get(1))){
						Long commodityResult =  commodityMoneyMap.get(profitArray.get(1)) + Long.parseLong(profitArray.get(2));
						commodityMoneyMap.put(profitArray.get(1), commodityResult);
					}
				}
			}
		} catch(IOException e){
			System.out.println("closeできませんでした。");
		} finally {
			pr.close();
		}
		//entrysetでmapの中身を確認
		System.out.println(moneyMap.entrySet());
		System.out.println(commodityMoneyMap.entrySet());
		//ファイルの出力
		try {
			//コマンドライン引数にファイルを作成
			List<Entry<String, Long>> list_entries = new ArrayList<Entry<String, Long>>(moneyMap.entrySet());
			Collections.sort(list_entries, new Comparator<Entry<String, Long>>() {
				public int compare(Entry<String, Long> obj1, Entry<String, Long> obj2)
	            {
	                //降順
	                return obj2.getValue().compareTo(obj1.getValue());
	            }
	        });
			File result = new File(args[0], "branch.out");
			result.createNewFile();
			FileWriter fw = new FileWriter(result);
			bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			//Mapに格納されているキーを参照して要素の数だけ処理をまわす
			 // 7. ループで要素順に値を取得する
	        for(Entry<String, Long> entry : list_entries) {
	            System.out.println(entry.getKey() + " : " + entry.getValue());
	            pw.println(entry.getKey() + "," + branchMap.get(entry.getKey()) + "," + moneyMap.get(entry.getKey()));
	        }
			//ファイルを閉じる
			pw.close();
		} catch(IOException e){
			System.out.println("closeできませんでした。");
		} finally {
			bw.close();
		}
		try {
			//降順にソートするためにlistを用意
			List<Entry<String, Long>> clist_entries = new ArrayList<Entry<String, Long>>(commodityMoneyMap.entrySet());
			Collections.sort(clist_entries, new Comparator<Entry<String, Long>>() {
				public int compare(Entry<String, Long> obj1, Entry<String, Long> obj2)
	            {
	                //降順
	                return obj2.getValue().compareTo(obj1.getValue());
	            }
	        });
			//コマンドライン引数にファイルを作成
			File cresult = new File(args[0], "commodity.out");
			cresult.createNewFile();
			FileWriter cfw = new FileWriter(cresult);
			cbw = new BufferedWriter(cfw);
			PrintWriter cpw = new PrintWriter(cbw);
			//Mapに格納されているキーを参照して要素の数だけ処理をまわす
			 // ループで要素順に値を取得する
	        for(Entry<String, Long> centry : clist_entries) {
	            System.out.println(centry.getKey() + " : " + centry.getValue());
	            cpw.println(centry.getKey() + "," + commodityMap.get(centry.getKey()) + "," + commodityMoneyMap.get(centry.getKey()));
	        }
			//ファイルを閉じる
			cpw.close();
		} catch(IOException e){
			System.out.println("closeできませんでした。");
		} finally {
			cbw.close();
		}
	}
}



